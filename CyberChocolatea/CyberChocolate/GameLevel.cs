﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace CyberChocolate
{
    class GameLevel : DrawableGameComponent
    {
        public int Row;
        private List<Obstacle> Obstacles = new List<Obstacle>();
        SpriteBatch spriteBatch;
        Texture2D Texture;
        Texture2D Background;
        Matrix matrix;
        public static GameLevel CurrentGameLevel { get; private set; }
        public  GameLevel(Game game,int row) : base(game)
        {
            Row = row;
            GameLevel.CurrentGameLevel = this;
        }

        public void getMatrix(Matrix matrix)
        {
            this.matrix = matrix;
        }
        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Texture = Game.Content.Load<Texture2D>("Obstacles/Box");
            Background = Game.Content.Load<Texture2D>("Background/background");
        }
        public override void Draw(GameTime time)
        {
            
            for (int x = 0; x < Row; x++)
            {
               
                    Vector2 tilePosition =  new Vector2(x * Texture.Width, 450);
                Obstacles.Add(new Obstacle(tilePosition, Texture, spriteBatch));
                Vector2 tilePosition2 = new Vector2(650, x * Texture.Height);
                Obstacles.Add(new Obstacle(tilePosition2, Texture, spriteBatch));

            }
            Obstacles.Add(new Obstacle(new Vector2(300, 405), Texture, spriteBatch));
            Obstacles.Add(new Obstacle(new Vector2(500, 305), Texture, spriteBatch));
            Obstacles.Add(new Obstacle(new Vector2(570, 305), Texture, spriteBatch));
            Obstacles.Add(new Obstacle(new Vector2(600, 505), Texture, spriteBatch));

             // spriteBatch.Begin();
             spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, matrix);
            spriteBatch.Draw(Background, new Vector2(0, 0), Color.White);
            foreach (var Obstacle in Obstacles)
            {
                Obstacle.Draw(spriteBatch);
            }
            spriteBatch.End();

        }
        public bool HasRoomForRectangle(Rectangle rectangleToCheck)
        {
            foreach (var tile in Obstacles)
            {
                if (!Object.ReferenceEquals(tile,null))
                {
                    if (tile.Bounds.Intersects(rectangleToCheck))
                    {
                        return false;
                    }
                }
                
            }

            return true;
        }

 
        
    }

}
