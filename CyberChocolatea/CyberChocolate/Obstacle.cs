﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace CyberChocolate
{
    class Obstacle
    {
        private SpriteBatch spriteBatch;
        public Vector2 Position { get; set; }
        public Texture2D Texture { set; get; }
        private static Texture2D rect;
        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y,
                  Texture.Width, Texture.Height);
            }
        }


        public Obstacle(Vector2 position, Texture2D texture,SpriteBatch spriteBatch)
        {
             Texture = texture;
             Position = position;
            this.spriteBatch = spriteBatch;

        }
        
        private void DrawRectangle(Rectangle coords, Color color, GraphicsDevice device)
        {
            if (rect == null)
            {
                rect = new Texture2D(device, 1, 1);
                rect.SetData(new[] { Color.White });
            }
            spriteBatch.Draw(rect, coords, color);
        }
        



        public  void Draw(SpriteBatch spri)
        {

            //  DrawRectangle(new Rectangle((int)Position.X, (int)Position.Y, Texture.Height+10, Texture.Width+10), Color.Red,device);
            spri.Draw(Texture, Position, Color.White);


        }
    }
}
