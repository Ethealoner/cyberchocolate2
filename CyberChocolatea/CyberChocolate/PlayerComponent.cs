﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpriterDotNet.MonoGame;
using SpriterDotNet;
using SpriterDotNet.MonoGame.Content;
using SpriterDotNet.Providers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace CyberChocolate
{
    class PlayerComponent : DrawableGameComponent
    {
        private GameAnimator currentAnimator;
        private SpriteBatch spriteBatch;
        private IList<GameAnimator> animators = new List<GameAnimator>();
        private Vector2 movement;
        private Vector2 gravity;
        enum characterState { idle,idle_reverse, walking, walking_reverse, running, running_reverse };
        private characterState state;
        private bool facingRight;
        private bool running;
        private bool hasJumped;
        private int speed = 80;
        private int jumpHeight =  0;
        private Matrix matrix;
        private SpriteFont debugFont;
        public void getMatrix(Matrix matrix)
        {
            this.matrix = matrix;
        }




        public PlayerComponent(Game game) : base(game)
        {
           state = characterState.idle;
            facingRight = true;
            running = true;
            hasJumped = false;
            movement = new Vector2();
            gravity = new Vector2();
            
            
        }

        private static readonly IList<string> Scmls = new List<string>
        {
            "Elidia Spriter/Elidia"
        };

        

        private static readonly Config config = new Config
        {
            MetadataEnabled = false,
            EventsEnabled = true,
            PoolingEnabled = true,
            TagsEnabled = true,
            VarsEnabled = true,
            SoundsEnabled = false
        };

        public override void Initialize()
        {
            base.Initialize();
            

        }

        protected override void LoadContent()
        {
           
            base.LoadContent();
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            DefaultProviderFactory<ISprite, SoundEffect> factory = new DefaultProviderFactory<ISprite, SoundEffect>(config, true);
           // debugFont = Game.Content.Load<SpriteFont>("Font");

            foreach (string scmlpath in Scmls)
            {
                SpriterContentLoader loader = new SpriterContentLoader(Game.Content, scmlpath);
                Stack<SpriteDrawInfo> drawInfoPool = new Stack<SpriteDrawInfo>();

                loader.Fill(factory);
                foreach (SpriterEntity entity in loader.Spriter.Entities)
                {
                    
                    var animator = new GameAnimator(entity,1183,3772, Game.GraphicsDevice,factory);
                    animators.Add(animator);

                }

            }

            

            currentAnimator = animators.First();
            Vector2 scale = new Vector2 ((float)0.07);
            currentAnimator.Scale = scale;
            currentAnimator.Position = new Vector2 (100,100);
          
        }

        public override void Draw(GameTime gametime)
        {
            // spriteBatch.Begin(SpriteSortMode.BackToFront);
            currentAnimator.getMatrix(matrix);
            currentAnimator.Draw(spriteBatch);
            //spriteBatch.End();

        }
        public bool IsOnFirmGround()
        {
            Rectangle onePixelLower = currentAnimator.getRectangle() ;
            onePixelLower.Offset(-2, 5);
            if (!GameLevel.CurrentGameLevel.HasRoomForRectangle(onePixelLower))
            {
              //  hasJumped = false;
                return true;
            }                
            else               
                return false;
        }

        public void DrawDebug()
        {
            string positionInText =
        string.Format("Position of Jumper: ({0:0.0}, {1:0.0})", currentAnimator.Position.X, currentAnimator.Position.Y);
            string movementInText =
        string.Format("Current movement: ({0:0.0}, {1:0.0})", movement.X, movement.Y);
            spriteBatch.DrawString(debugFont, positionInText, new Vector2(10, 0), Color.White);
            spriteBatch.DrawString(debugFont, movementInText, new Vector2(10, 20), Color.White);

        }


        private void PlayerState()
        {
            switch (state)
            {
                case characterState.idle:
                        currentAnimator.Play("Idle");
                    break;
                case characterState.idle_reverse:
                        currentAnimator.Play("Idle_Reverse");
                    break;
                case characterState.walking:
                        currentAnimator.Play("Walking");
                    break;
                case characterState.walking_reverse:
                        currentAnimator.Play("Walking_Reverse");
                    break;
                case characterState.running:
                        currentAnimator.Play("Running");
                    break;
                case characterState.running_reverse:
                    currentAnimator.Play("Running_Reverse");
                    break;                
                                    
            }

        }

        public void UpdateMovement(GameTime time)
        {
            currentAnimator.Position += movement * speed * (float)time.ElapsedGameTime.TotalSeconds;
            movement = new Vector2();

            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                running = !running;              
            }
            
            if (Keyboard.GetState().IsKeyDown(Keys.D) && running == false)
            {
                if (state != characterState.walking)
                {
                    facingRight = true;
                    state = characterState.walking;
                    PlayerState();
                }
                movement = new Vector2(1, 0);
                // movement = new Vector2(currentAnimator.Position.X + (speed * (float)time.ElapsedGameTime.TotalSeconds), currentAnimator.Position.Y);
                // currentAnimator.Position = movement;
            }
            
            if (Keyboard.GetState().IsKeyDown(Keys.D) && running == true)
            {
                if (state != characterState.running)
                {
                    facingRight = true;
                    state = characterState.running;
                    PlayerState();
                }
                movement = new Vector2(2, 0);
                //currentAnimator.Position = movement;
            }
            
            if (Keyboard.GetState().IsKeyDown(Keys.A) && running == true)
            {
                if (state != characterState.running_reverse)
                {
                    facingRight = false;
                    state = characterState.running_reverse;
                    PlayerState();
                }
                movement = new Vector2(-2, 0);
                // movement = new Vector2(currentAnimator.Position.X - ((speed * (float)2.0) * (float)time.ElapsedGameTime.TotalSeconds), currentAnimator.Position.Y);
                // currentAnimator.Position = movement;
            }
            
            if (Keyboard.GetState().IsKeyDown(Keys.A) && running == false)
            {
                if (state != characterState.walking_reverse)
                {
                    facingRight = false;
                    state = characterState.walking_reverse;
                    PlayerState();
                }
                movement = new Vector2(-1, 0);
                // movement = new Vector2(currentAnimator.Position.X - (speed * (float)time.ElapsedGameTime.TotalSeconds), currentAnimator.Position.Y);
                // currentAnimator.Position = movement;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.W) && IsOnFirmGround())
{
                hasJumped = true;
               // movement = new Vector2(0, -100);
            }
            else
            {
                if (!(Keyboard.GetState().IsKeyDown(Keys.A)) && !(Keyboard.GetState().IsKeyDown(Keys.D)))
                {
                    if (state != characterState.idle && state != characterState.idle_reverse)
                    {
                        if (facingRight == true)
                        {
                            state = characterState.idle;
                        }
                        else
                        {
                            state = characterState.idle_reverse;
                        }

                        PlayerState();
                    }

                }

            }
            
    

        }

        public void GravityForce()
        {

            if (!IsOnFirmGround() && !hasJumped) {
                gravity += new Vector2(0, 0.10f);

                currentAnimator.Position += gravity;
            }else if(IsOnFirmGround() && !hasJumped)
            {
                gravity = new Vector2(0, 0);
            }
            if (hasJumped)
            {

                gravity = new Vector2(0, -5f);
                jumpHeight += (int)gravity.Y;
                currentAnimator.Position += gravity;
                if(jumpHeight < 2)
                {
                    jumpHeight = 0;
                    hasJumped = false;
                }
            }


        }

        public override void Update(GameTime gameTime)
        {
            
             Vector2 oldPosition = currentAnimator.Position;
              UpdateMovement(gameTime);
             if (!GameLevel.CurrentGameLevel.HasRoomForRectangle(currentAnimator.getRectangle()))
              {
                  currentAnimator.Position = oldPosition;
               }
                GravityForce();
               



                currentAnimator.Update(gameTime.ElapsedGameTime.Milliseconds);
        }

    }
}
